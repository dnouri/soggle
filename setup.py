import os

from setuptools import setup, find_packages

version = '0.1dev'

here = os.path.abspath(os.path.dirname(__file__))
try:
    README = open(os.path.join(here, 'README.rst')).read()
    CHANGES = open(os.path.join(here, 'CHANGES.txt')).read()
except IOError:
    README = CHANGES = ''

install_requires = [
    'docopt',
    'nltk',
    'pandas',
    'pytest',
    'scikit-learn==0.12'  # may try >=0.13dev
    ]

maybe_package_manager = [
    'matplotlib',
    'numpy',
    'scipy',
    ]

if os.environ.get('SOGGLE_INSTALL_ALL_DEPS', '0').lower() in ('1', 'y'):
    install_requires += maybe_package_manager


setup(name='soggle',
      version=version,
      description="Code for the Predict Closed Questions on Stack Overflow "
      "challenge on kaggle.",
      long_description='\n\n'.join([README, CHANGES]),
      classifiers=[
          'Development Status :: 3 - Alpha',
        ],
      keywords='',
      author='Daniel Nouri',
      author_email='daniel.nouri@gmail.com',
      url='https://github.com/dnouri/soggle',
      license='MIT',
      packages=find_packages(),
      include_package_data=True,
      zip_safe=False,
      install_requires=install_requires,
      entry_points="""
      [console_scripts]
      soggle-introspect = soggle.introspect:main
      soggle-learn = soggle.learn:main
      soggle-eval = soggle.evaluate:main
      """,
      dependency_links=[
          'http://github.com/scikit-learn/scikit-learn'
          '/tarball/master#egg=scikit-learn-0.13dev',
          ]
      )
