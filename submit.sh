#!/usr/bin/env sh

TRAIN_FILE=data/train-sample-reverse.csv
TEST_FILE=data/train-30k.csv
SUBMIT_TEST_FILE=data/public_leaderboard.csv
FULL_TRAIN_FILE=data/train.csv
MODEL_FILE=model-sample-reverse.pkl
SUBMISSION_FILE=submission.csv
TRUNCATE=0.147,1.0


echo "soggle-learn train"
echo "~~~~~~~~~~~~~~~~~~"
bin/soggle-learn train $TRAIN_FILE $MODEL_FILE --truncate=$TRUNCATE

echo "soggle-eval logloss"
echo "~~~~~~~~~~~~~~~~~~~"
bin/soggle-eval logloss $TEST_FILE $MODEL_FILE \
    --full-train-file=$FULL_TRAIN_FILE \
    --train-file=$TRAIN_FILE

echo "soggle-eval submit"
echo "~~~~~~~~~~~~~~~~~~"
bin/soggle-eval submit $SUBMIT_TEST_FILE $MODEL_FILE $SUBMISSION_FILE \
        --full-train-file=$FULL_TRAIN_FILE \
        --train-file=$TRAIN_FILE
