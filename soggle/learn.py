"""Model training.

Usage:
  soggle-learn eval_model DATASET_FILE [--models=<n>] [options]
  soggle-learn train DATASET_FILE CLF_OUTFILE [options]
  soggle-learn grid_search_base DATASET_FILE [options]
  soggle-learn grid_search_basecharngrams DATASET_FILE [options]
  soggle-learn grid_search_multinomialnb DATASET_FILE [options]
  soggle-learn grid_search_randomforest DATASET_FILE [options]
  soggle-learn grid_search_sgd DATASET_FILE [options]
  soggle-learn grid_search_sgdcharngrams DATASET_FILE [options]
  soggle-learn learning_curve DATASET_FILE [--model=<n>] [options]

Options:
  -h --help           Show this screen
  --truncate=t        Truncate dataset [default: 0.0,1.0]
  --pdb               Do post mortem debugging on errors
  --profile=<fn>      Save a profile to <fn>
"""

import cPickle
from pprint import pprint

import matplotlib.pyplot as pl
from sklearn.base import clone
from sklearn.base import BaseEstimator
from sklearn.cross_validation import ShuffleSplit
from sklearn.externals.joblib import delayed
from sklearn.externals.joblib import Parallel
from sklearn.grid_search import GridSearchCV
from sklearn.metrics import classification_report
from sklearn.metrics import confusion_matrix
from sklearn.metrics import f1_score
import numpy as np

from .evaluate import multiclass_logloss
from .models import AveragingEstimator
from .models import BaseCharNGramsModel
from .models import BaseCharNGramsModelWithoutWords2
from .models import BaseModel
from .models import MultinomialNBModel
from .models import RandomForestModel
from .models import SGDCharNGramsModel
from .models import SGDModel
from .util import Command
from .util import timed


def make_cv(n_samples):
    return ShuffleSplit(
        n_samples,
        n_iterations=3,
        test_size=0.25,
        indices=True,
        random_state=43,
        )


def _make_some_models():
    return [
        BaseCharNGramsModel(
            features__words__select__k=20000,
            clf__C=4.0,
            )(),

        BaseCharNGramsModel(
            features__words__count__analyzer='char_wb',
            features__words__select__k=20000,
            clf__C=4.0,
            )(),

        BaseCharNGramsModelWithoutWords2(
            features__words__count__ngram_range=(2, 3),
            features__words__select__k=15000,
            clf__C=4.0,
            )(),

        BaseModel(
            features__words__select__k=15000,
            clf__C=4.5,
            )(),

        BaseModel(
            features__words__count__ngram_range=(1, 3),
            features__words__count__max_features=60000,
            features__words__select__k=20000,
            clf__C=4.5,
            )(),

        # MultinomialNBModel()(),

        # RandomForestModel()(),

        # XXX: Note that using a SGD classifier together with anything
        #      else will result in a freeze.  SGDModels combined (without
        #      the chi2 score function) will still work though:

        # SGDModel()(),

        # SGDCharNGramsModel()(),

        # SGDCharNGramsModel(
        #     features__words__count__analyzer='char_wb',
        #     )(),
        ]


def _eval_model(i, clf, X_train, y_train, X_test, y_test):
    clf = clone(clf)
    with timed("    training model %s ..." % i, print_when_done=True):
        clf.fit(X_train, y_train)

    train_score = f1_score(
        y_train[:3000],
        clf.predict_proba(X_train).argmax(axis=1)[:3000],
        )
    probs = clf.predict_proba(X_test)
    f_score = f1_score(y_test, probs.argmax(axis=1))
    ll_score = multiclass_logloss(y_test, probs)

    print "  Model %s" % i
    print "   + f1-score (train):   %0.4f" % train_score
    print "   + f1-score:           %0.4f" % f_score
    print "   + log loss:           %0.4f" % ll_score
    return probs


def eval_model(dataset):
    models = _make_some_models()

    n_models = int((main.arguments['--models'] or [99])[0])

    cv = make_cv(len(dataset.data))
    f_scores = []
    ll_scores = []

    for i, (train, test) in enumerate(cv):
        print "Iter %s:" % (i + 1)
        print "-------"
        probs_iter = np.zeros((cv.n_test, dataset.n_classes))

        models = models[:n_models]

        X_train, y_train = dataset.data[train], dataset.target[train]
        X_test, y_test = dataset.data[test], dataset.target[test]

        probs = Parallel(n_jobs=-1, verbose=0)(
            delayed(_eval_model)(
                    j, clf,
                    X_train, y_train,
                    X_test, y_test,
                    )
            for j, clf in enumerate(models))

        for prob in probs:
            probs_iter += prob
        probs_iter /= float(len(models))

        ll_scores.append(multiclass_logloss(y_test, probs_iter))
        f_scores.append(f1_score(y_test, probs_iter.argmax(axis=1)))

        print
        print "Iter %s:" % (i + 1)
        print "   = combined f1 score   %0.4f" % f_scores[-1]
        print "   = combined logloss:   %0.4f" % ll_scores[-1]
        print

    print "mean f1 score: %0.4f  (std %0.4f)" % (
        np.mean(f_scores), np.std(f_scores))
    print "mean ll score: %0.4f  (std %0.4f)" % (
        np.mean(ll_scores), np.std(ll_scores))
    print "==================================="
    print

    pred = probs_iter.argmax(axis=1)

    print "classification report for the last iteration:"
    print classification_report(y_test, pred, target_names=dataset.categories)

    print "confusion matrix for the last iteration:"
    print confusion_matrix(y_test, pred)
    print


def train(dataset):
    dataset.shuffle()
    outfile = main.arguments['CLF_OUTFILE']

    clf = AveragingEstimator(_make_some_models(), verbose=4, n_jobs=1)
    with timed("  Training model ..."):
        clf.fit(dataset.data, dataset.target)

    train_score = f1_score(
        dataset.target[:1000],
        clf.predict_proba(dataset.data).argmax(axis=1)[:1000],
        )
    print "   + f1-score (train):   %0.4f" % train_score

    with open(outfile, 'w') as f:
        cPickle.dump(clf, f)
    print "Written %s to disk." % outfile


def grid_search_base(dataset):
    pipeline = BaseModel()()
    parameters = dict(
        # C=4.7 gets -1.0557 for small-2.csv
        clf__C=[4.4, 4.7, 5.0],
        )
    grid_search(dataset, pipeline, parameters)


def grid_search_basecharngrams(dataset):
    pipeline = BaseCharNGramsModel()()
    parameters = dict(
        # C=4.0 gets -1.0382 for small-2.csv
        clf__C=(3.0, 4.0, 5.0),
        )
    grid_search(dataset, pipeline, parameters)


def grid_search_multinomialnb(dataset):
    pipeline = MultinomialNBModel()()
    parameters = dict(
        features__words__select__k=(1000, 3000, 5000, 8000),
        clf__alpha=(0.0, 0.01, 0.03, 0.1, 0.3, 1.0, 10),
        )
    grid_search(dataset, pipeline, parameters)


def grid_search_randomforest(dataset):
    pipeline = RandomForestModel()()
    parameters = dict(
        clf__n_estimators=(20, 50),
        )
    grid_search(dataset, pipeline, parameters)


def grid_search_sgd(dataset):
    pipeline = SGDModel()()
    parameters = dict(
        clf__estimator__alpha=(0.0003, 0.001, 0.003),
        )
    grid_search(dataset, pipeline, parameters)


def grid_search_sgdcharngrams(dataset):
    pipeline = SGDCharNGramsModel()()
    parameters = dict(
        features__words__select__k=(30000, 40000),
        clf__estimator__alpha=(0.0003, 0.001),
        )
    grid_search(dataset, pipeline, parameters)


def grid_search(dataset, pipeline, parameters):
    # http://scikit-learn.org/stable/modules/grid_search.html

    grid_search = GridSearchCV(
        pipeline,
        parameters,
        cv=make_cv(len(dataset.data)),
        loss_func=multiclass_logloss,
        verbose=4,
        n_jobs=-1,
        )
    print "Performing grid search..."
    print "pipeline:", [name for name, _ in pipeline.steps]
    print "parameters:"
    pprint(parameters)
    with timed("Grid search ..."):
        grid_search.fit(dataset.data, dataset.target)

    # The rest is reporting on the best resut:
    print
    print "== " * 20
    print "All parameters:"
    best_parameters = grid_search.best_estimator_.get_params()
    for param_name, value in sorted(best_parameters.items()):
        if not isinstance(value, BaseEstimator):
            print "    %s=%r," % (param_name, value)

    print
    print "== " * 20
    print "Best score: %0.4f" % grid_search.best_score_
    print "Best grid parameters:"
    for param_name in sorted(parameters.keys()):
        print "    %s=%r," % (param_name, best_parameters[param_name])
    print "== " * 20


def learning_curve(dataset):
    cv = make_cv(len(dataset.data))
    train, test = iter(cv).next()
    X_train, y_train = dataset.data[train], dataset.target[train]
    X_test, y_test = dataset.data[test], dataset.target[test]

    idx = int((main.arguments['--model'] or [0])[0])
    classifier = _make_some_models()[idx]

    scores_train = []
    scores_test = []
    sizes = []

    for frac in np.arange(0.05, 1.05, 0.1):
        frac_size = X_train.shape[0] * frac
        sizes.append(frac_size)
        X_train1 = X_train[:frac_size]
        y_train1 = y_train[:frac_size]

        clf = clone(classifier)
        with timed("  Training model with %s examples ..." % len(X_train1)):
            clf.fit(X_train1, y_train1)

        probs_train = clf.predict_proba(X_train1)
        probs_test = clf.predict_proba(X_test)

        score_train = f1_score(y_train1, probs_train.argmax(axis=1))
        score_test = f1_score(y_test, probs_test.argmax(axis=1))
        ll_train = multiclass_logloss(y_train1, probs_train)
        ll_test = multiclass_logloss(y_test, probs_test)

        scores_train.append(score_train)
        scores_test.append(score_test)
        print "                  train      test"
        print "   + f1-score    %0.4f    %0.4f" % (score_train, score_test)
        print "   + log loss    %0.4f    %0.4f" % (ll_train, ll_test)

    pl.plot(
        sizes, scores_train, 'b',
        sizes, scores_test, 'r',
        )
    pl.title("Learning curve for model %s" % idx)
    pl.show()


class Learn(Command):
    __doc__ = __doc__

    funcs = (
        eval_model,
        train,
        grid_search_base,
        grid_search_basecharngrams,
        grid_search_multinomialnb,
        grid_search_randomforest,
        grid_search_sgd,
        grid_search_sgdcharngrams,
        learning_curve,
        )

main = Learn()
