"""Model evaluation.

Usage:
  soggle-eval logloss DATASET_FILE CLF_INFILE [--full-train-file=<f> [--train-file=<f>]] [options]
  soggle-eval submit DATASET_FILE CLF_INFILE OUTFILE [--full-train-file=<f> [--train-file=<f>]] [options]

Options:
  -h --help           Show this screen
  --truncate=t        Truncate dataset [default: 0.0,1.0]
  --pdb               Do post mortem debugging on errors
  --profile=<fn>      Save a profile to <fn>
"""

import cPickle

import numpy as np

from sklearn.metrics import classification_report
from sklearn.metrics import confusion_matrix
from sklearn.metrics import f1_score
from .util import Command
from .competition_utilities import cap_and_update_priors
from .competition_utilities import get_priors
from .competition_utilities import write_submission


def multiclass_logloss(actual, predicted, eps=1e-15):
    """Multi class version of Logarithmic Loss metric.
    http://www.kaggle.com/c/emc-data-science/forums/t/2149/is-anyone-noticing-difference-betwen-validation-and-leaderboard-error/12209#post12209
    """
    # Convert 'actual' to a binary array if it's not already:
    if len(actual.shape) == 1:
        actual2 = np.zeros((actual.shape[0], predicted.shape[1]))
        for i, val in enumerate(actual):
            actual2[i, val] = 1
        actual = actual2

    clip = np.clip(predicted, eps, 1 - eps)
    rows = actual.shape[0]
    vsota = np.sum(actual * np.log(clip))
    return -1.0 / rows * vsota


def _calc_probas(dataset):
    infile = main.arguments['CLF_INFILE']
    with open(infile) as f:
        clf = cPickle.load(f)

    probas = clf.predict_proba(dataset.data)

    if main.arguments['--full-train-file']:
        full_train_file = main.arguments['--full-train-file'][0]
        new_priors = np.array(get_priors(full_train_file))
        if main.arguments['--train-file']:
            train_file = main.arguments['--train-file'][0]
            old_priors = np.array(get_priors(train_file))
        else:
            old_priors = np.ones(new_priors.shape) / new_priors.shape[0]
        probas = cap_and_update_priors(old_priors, probas, new_priors, 0.001)

    return probas


def logloss(dataset):
    probas = _calc_probas(dataset)
    pred = probas.argmax(axis=1)

    print "classification report:"
    print classification_report(
        dataset.target, pred, target_names=dataset.categories)

    print "confusion matrix:"
    print confusion_matrix(dataset.target, pred)

    score = f1_score(dataset.target, pred)
    print
    print "   + f1-score        :   %0.4f" % score

    ll = multiclass_logloss(dataset.target, probas)
    print "   = log loss        :   %0.4f" % ll
    print


def submit(dataset):
    probas = _calc_probas(dataset)
    write_submission(main.arguments['OUTFILE'], probas)
    print "Written submission file %s to disk." % main.arguments['OUTFILE']


class Evaluate(Command):
    __doc__ = __doc__

    funcs = (
        logloss,
        submit,
        )

main = Evaluate()
