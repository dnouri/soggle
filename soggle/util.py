from contextlib import contextmanager
from functools import wraps
import hashlib
import cPickle
import os
import cProfile
import pdb
import random
import string
import sys
from time import time
import traceback
import warnings

from docopt import docopt


@contextmanager
def timed(message, print_when_done=False):
    if not print_when_done:
        print message,
        sys.stdout.flush()
    t0 = time()
    yield
    dur = time() - t0
    if dur > 180:
        dur_str = '%0.1fmin' % (dur / 60)
    else:
        dur_str = '%0.1fs' % dur
    if print_when_done:
        print message,
    print "done in %s" % dur_str


CACHE_PATH = 'cache/'
if not os.path.exists(CACHE_PATH):
    os.mkdir(CACHE_PATH)


def default_cache_key(*args, **kwargs):
    return str(args) + str(kwargs)


def cached(cache_key=default_cache_key):
    def cached(func):
        @wraps(func)
        def wrapper(*args, **kwargs):

            # Calculation of the cache key is delegated to a function
            # that's passed in via the decorator call
            # (`default_cache_key` by default).
            key = cache_key(*args, **kwargs)
            hashed_key = hashlib.sha1(key).hexdigest()[:8]

            # We construct the filename using the cache key.  If the
            # file exists, unpickle and return the value.
            filename = os.path.join(
                CACHE_PATH,
                '{}.{}-cache-{}.pkl'.format(
                    func.__module__, func.__name__, hashed_key))

            if os.path.exists(filename):
                filesize = os.path.getsize(filename)
                size = "%0.1f MB" % (filesize / (1024 * 1024.0))
                # print " * cache hit: %s (%s)" % (filename, size)
                with open(filename, 'r') as f:
                    return cPickle.load(f)
            else:
                # print " * cache miss: %s" % filename
                value = func(*args, **kwargs)
                tmp_filename = '{}-{}.tmp'.format(
                    filename,
                    ''.join(random.sample(string.ascii_letters, 4)),
                    )
                with open(tmp_filename, 'wb') as f:
                    try:
                        cPickle.dump(value, f, -1)
                        os.rename(tmp_filename, filename)
                    except SystemError:
                        warnings.warn(
                            "Saving pickle %s resulted in SystemError" %
                            filename,
                            )
                return value

        return wrapper
    return cached


class Command(object):
    def __call__(self):
        from .features import Dataset

        arguments = docopt(self.__doc__, argv=sys.argv[1:])
        self.arguments = arguments

        start, end = arguments['--truncate'].split(',')
        start, end = float(start), float(end)
        dataset = Dataset(
            arguments['DATASET_FILE'],
            truncate_start=start, truncate_end=end,
            )

        for func in self.funcs:
            if arguments[func.__name__]:
                break

        # If profiling, wrap the function with another one that does the
        # profiling:
        if arguments['--profile']:
            func_ = func

            def prof(dataset):
                cProfile.runctx(
                    'func(dataset)',
                    globals(),
                    {'func': func_, 'dataset': dataset},
                    filename=arguments['--profile'],
                    )
            func = prof

        # If debugging, call pdb.post_mortem() in the except clause:
        try:
            func(dataset)
        except:
            if arguments['--pdb']:
                traceback.print_exc()
                pdb.post_mortem(sys.exc_traceback)
            else:
                raise
