"""Introspect the contents of one or more dataset files.

Usage:
  soggle-introspect [options] FILE ...

Options:
  -h --help           Show this screen
  --truncate=t        Truncate dataset [default: 0.0,1.0]
"""

import sys

from docopt import docopt
import numpy as np


def print_stats(df):
    if "OpenStatus" in df:
        groupby = df.groupby(df['OpenStatus'])
    else:
        groupby = (('n/a', df),)

    for (name, group) in groupby:
        print "%-20s: %10s" % (name, len(group)),
        dates = np.array(group['PostCreationDate'])
        dates.sort()
        print "  %s - %s" % (
            dates[0].strftime("%Y-%m-%d %H:%M"),
            dates[-1].strftime("%Y-%m-%d %H:%M"),
            )

    print
    print "%-20s: %10s" % ('TOTAL', len(df))
    print


def main():
    from .features import Dataset

    arguments = docopt(__doc__, argv=sys.argv[1:])

    start, end = arguments['--truncate'].split(',')
    start, end = float(start), float(end)

    for filename in arguments['FILE']:
        print filename
        print "=" * len(filename)
        Dataset(
            filename,
            truncate_start=start, truncate_end=end,
            )
