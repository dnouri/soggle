from collections import Counter

import numpy as np
from scipy.sparse import coo_matrix
from sklearn.base import BaseEstimator
from sklearn.decomposition import RandomizedPCA
from sklearn.externals.joblib import delayed
from sklearn.externals.joblib import Parallel
from sklearn.ensemble import RandomForestClassifier
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.feature_extraction.text import TfidfTransformer
from sklearn.feature_selection import chi2
from sklearn.feature_selection import f_classif
from sklearn.feature_selection import SelectKBest
from sklearn.linear_model import LogisticRegression
from sklearn.linear_model import SGDClassifier
from sklearn.multiclass import OneVsRestClassifier
from sklearn.naive_bayes import MultinomialNB
from sklearn.pipeline import Pipeline
from sklearn.preprocessing import Scaler

from .features import FeatureStacker
from .features import DensifyTransformer
from .features import LanguageFeatures
from .features import PostFeatures
from .features import TextFeatures
from .features import TextExtractor
from .util import cached


class AbstractModel(object):
    default_params = dict()

    @property
    def pipeline(self):
        raise NotImplementedError()

    def __init__(self, **kwargs):
        params = self.default_params.copy()
        params.update(kwargs)
        self.params = params

    def __call__(self):
        pipeline = self.pipeline
        pipeline.set_params(**self.params)
        return pipeline


class ModelWithBaseFeatures(AbstractModel):
    @property
    def textfeatures(self):
        return Pipeline([
            ('textfeatures', TextFeatures()),
            ('scaler', Scaler(with_mean=False)),
            ])

    @property
    def languagefeatures(self):
        return Pipeline([
            ('languagefeatures', LanguageFeatures()),
            ('count', LargeCountVectorizer(ngram_range=(1, 3))),
            ('scaler', Scaler(with_mean=False)),
            ('pca', RandomizedPCA(n_components=20)),
            ])

    @property
    def postfeatures(self):
        return Pipeline([
            ('postfeatures', PostFeatures()),
            ('scaler', Scaler(with_mean=False)),
            ])

    @property
    def words(self):
        return Pipeline([
            ('extract', TextExtractor()),
            ('count', LargeCountVectorizer()),
            ('tfidf', CachedTfidfTransformer()),
            ('select', SelectKBest(score_func=chi2)),
            ])

    @property
    def words2(self):
        return Pipeline([
            ('extract', TextExtractor()),
            ('count', LargeCountVectorizer()),
            ('tfidf', CachedTfidfTransformer()),
            ('select', SelectKBest(score_func=chi2, k=65)),
            ])

    @property
    def words_wordlist(self):
        count = FileVocabCountVectorizer(vocabulary='wordlists/bad.txt')

        return Pipeline([
            ('extract', TextExtractor()),
            ('count', count),
            ('tfidf', CachedTfidfTransformer()),
            ('select', SelectKBest(score_func=chi2, k=100)),
            ])

    @property
    def features(self):
        return FeatureStacker([
            ("words", self.words),
            ("words2", self.words2),
            ("textfeatures", self.textfeatures),
            ("postfeatures", self.postfeatures),
#            ("words_wordlist", self.words_wordlist),
#            ('languagefeatures', self.languagefeatures),
            ])


class BaseModel(ModelWithBaseFeatures):
    # scores around 0.665 for train-sample-reverse.csv
    default_params = dict(
        features__words__count__ngram_range=(1, 2),
        features__words__count__max_df=0.9,
        features__words__count__min_df=0.0001,
        features__words__tfidf__norm='l2',
        features__words__select__k=10000,
        features__words2__count__max_features=100000,
        features__words2__select__k=65,
        clf__C=4.5,
        clf__penalty='l2',
        clf__class_weight='auto',
        )

    @property
    def pipeline(self):
        return Pipeline([
            ('features', self.features),
            ('clf', LogisticRegression()),
            ])


class BaseCharNGramsModel(BaseModel):
    default_params = dict(
        features__words__count__analyzer='char',
        features__words__count__ngram_range=(3, 4),
        features__words__count__max_features=80000,  # for character n-grams
        features__words__tfidf__norm='l2',
        features__words__tfidf__sublinear_tf=True,
        features__words__select__k=10000,
        features__words2__count__max_features=30000,
        features__words2__select__k=65,
        clf__C=4.0,
        clf__penalty='l2',
        clf__class_weight='auto',
        )


class BaseCharNGramsModelWithoutWords2(BaseCharNGramsModel):
    default_params = BaseCharNGramsModel.default_params.copy()
    del default_params['features__words2__count__max_features']
    del default_params['features__words2__select__k']

    @property
    def features(self):
        return FeatureStacker([
            ("words", self.words),
            ("textfeatures", self.textfeatures),
            ("postfeatures", self.postfeatures),
            ])


class MultinomialNBModel(ModelWithBaseFeatures):
    default_params = dict(
        features__words__select__k=3000,
        clf__alpha=10.0,
        )

    @property
    def words(self):
        return Pipeline([
            ('extract', TextExtractor()),
            ('count', LargeCountVectorizer()),
            ('select', SelectKBest(score_func=chi2)),
            ])

    @property
    def pipeline(self):
        return Pipeline([
            ('features', self.features),
            ('clf', MultinomialNB()),
            ])


class RandomForestModel(ModelWithBaseFeatures):
    default_params = dict(
        features__words__count__ngram_range=(1, 2),
        features__words__tfidf__norm='l2',
        features__words__select__k=1000,
        clf__compute_importances=True,
        clf__n_estimators=20,
        )

    @property
    def pipeline(self):
        clf = RandomForestClassifier()

        return Pipeline([
            ('features', self.features),
            ('densify', DensifyTransformer()),
            ('clf', clf),
            ])


class SGDModel(ModelWithBaseFeatures):
    default_params = dict(
        features__words__count__ngram_range=(1, 2),
        features__words__tfidf__norm='l2',
        features__words__select__k=10000,
        features__words__select__score_func=f_classif,  # will freeze with chi2
        clf__estimator__alpha=0.0003,
        clf__estimator__class_weight='auto',
        clf__estimator__n_iter=50,
        )

    @property
    def features(self):
        return FeatureStacker([
            ("words", self.words),
            ])

    @property
    def pipeline(self):
        clf = OneVsRestProbaClassifier(SGDClassifier(loss='log'))

        return Pipeline([
            ('features', self.features),
            ('clf', clf),
            ])


class SGDCharNGramsModel(SGDModel):
    default_params = dict(
        features__words__count__analyzer='char',
        features__words__count__lowercase=True,
        features__words__count__ngram_range=(3, 4),
        features__words__count__max_features=60000,  # for character n-grams
        features__words__tfidf__norm='l2',
        features__words__tfidf__sublinear_tf=True,
        features__words__select__k=40000,
        features__words__select__score_func=f_classif,  # will freeze with chi2
        clf__estimator__alpha=0.0003,
        clf__estimator__class_weight='auto',
        clf__estimator__n_iter=50,
        )


class OneVsRestProbaClassifier(OneVsRestClassifier):
    def predict_proba(self, X):
        # XXX: SGD.predict_proba doesn't handle sparse arrays well in
        #      scikit-learn 0.12.  Hack around it.
        def len(self):
            return self.shape[0]
        X.__len__.im_func.func_code = len.func_code

        probas = [e.predict_proba(X)[:, 1] for e in self.estimators_]
        probas = np.array(probas).T
        return probas


def _transform_cache_key_countvectorizer(self, X):
    return ','.join([
        str(X[:100]),
        str(X[-100:]),
        str(X.shape),
        str(sorted(self.get_params().items())),
#        str(sorted(self.vocabulary_.items())),
        ])


def _avgest_fit_est(est, i, X, y, verbose):
    if verbose:
        print "[AveragingEstimator] estimator_%s.fit() ..." % i
    return est.fit(X, y)


def _avgest_predict_proba(est, i, X, verbose):
    if verbose:
        print "[AveragingEstimator] estimator_%s.predict_proba() ..." % i
    return est.predict_proba(X)


class AveragingEstimator(BaseEstimator):
    n_jobs = 1  # BBB pickles

    def __init__(self, estimators, verbose=0, n_jobs=1):
        self.estimators = estimators
        self.verbose = verbose
        self.n_jobs = n_jobs

    def fit(self, X, y):
        result = Parallel(n_jobs=self.n_jobs, verbose=self.verbose)(
            delayed(_avgest_fit_est)(est, i, X, y, self.verbose)
            for i, est in enumerate(self.estimators))
        self.estimators = result
        return self

    def predict(self, X):
        return np.argmax(self.predict_proba(X), axis=1)

    def predict_proba(self, X):
        result = Parallel(n_jobs=self.n_jobs, verbose=self.verbose)(
            delayed(_avgest_predict_proba)(est, i, X, self.verbose)
            for i, est in enumerate(self.estimators))
        for proba in result[1:]:
            result[0] += proba
        return result[0] / len(self.estimators)


class FileVocabCountVectorizer(CountVectorizer):
    def __init__(self, vocabulary=None, **kwargs):
        self._vocabulary_filename = vocabulary
        if isinstance(vocabulary, basestring):
            vocabulary = self._make_vocabulary(vocabulary)
        super(FileVocabCountVectorizer, self).__init__(
            vocabulary=vocabulary, **kwargs)

    def _make_vocabulary(self, filename):
        with open(filename) as f:
            vocabulary = f.read().strip().splitlines()
            return dict((t, i) for i, t in enumerate(vocabulary))

    @property
    def vocabulary(self):
        return self._vocabulary_filename

    @vocabulary.setter
    def vocabulary(self, vocabulary):
        self._vocabulary_filename = vocabulary
        if isinstance(vocabulary, basestring):
            self.vocabulary_ = self._make_vocabulary(vocabulary)


class LargeCountVectorizer(CountVectorizer):
    def __init__(self, memory_efficient=True, **kwargs):
        self.memory_efficient = memory_efficient
        super(LargeCountVectorizer, self).__init__(**kwargs)

    @classmethod
    def _get_param_names(cls):
        return CountVectorizer._get_param_names() + ['memory_efficient']

    def fit_transform(self, raw_documents, y=None):
        """Learn the vocabulary dictionary and return the count vectors

        Works very similarly to the superclass method, but slower and
        more memory efficient.  We never keep all n-grams of all
        documents in memory.
        """
        if not self.memory_efficient:
            return super(
                LargeCountVectorizer, self).fit_transform(raw_documents, y)

        if self.fixed_vocabulary:
            return super(LargeCountVectorizer, self).fit_transform(
                raw_documents, y)

        self.vocabulary_ = {}

        # First round:
        # print "\n[LargeCountVectorizer] first round..."
        terms, stop_words = self._terms_and_stop_words(raw_documents)

        # store the learned stop words to make it easier to debug the value of
        # max_df
        self.max_df_stop_words_ = stop_words

        # store map from term name to feature integer index: we sort the term
        # to have reproducible outcome for the vocabulary structure: otherwise
        # the mapping from feature name to indices might depend on the memory
        # layout of the machine. Furthermore sorted terms might make it
        # possible to perform binary search in the feature names array.
        self.vocabulary_ = dict(((t, i) for i, t in enumerate(sorted(terms))))

        # Second round:
        # print "[LargeCountVectorizer] second round..."
        return self._term_counts_per_doc(raw_documents)

    def transform(self, raw_documents):
        if not self.memory_efficient:
            return super(
                LargeCountVectorizer, self).transform(raw_documents)

        # print "[LargeCountVectorizer] transform..."

        if not hasattr(self, 'vocabulary_') or len(self.vocabulary_) == 0:
            raise ValueError("Vocabulary wasn't fitted or is empty!")

        return self._term_counts_per_doc(raw_documents)

    @cached(_transform_cache_key_countvectorizer)
    def _terms_and_stop_words(self, raw_documents):
        analyze = self.build_analyzer()
        term_counts = Counter()
        document_counts = Counter()

        for doc in raw_documents:
            term_count_current = Counter(analyze(doc))
            term_counts.update(term_count_current)
            document_counts.update(term_count_current.iterkeys())

        n_doc = len(raw_documents)
        max_features = self.max_features
        max_df = self.max_df
        min_df = self.min_df

        max_doc_count = (max_df if isinstance(max_df, (int, np.integer))
                                else max_df * n_doc)
        min_doc_count = (min_df if isinstance(min_df,  (int, np.integer))
                                else min_df * n_doc)

        # filter out stop words: terms that occur in almost all documents
        if max_doc_count < n_doc or min_doc_count > 1:
            stop_words = set(t for t, dc in document_counts.iteritems()
                               if dc > max_doc_count or dc < min_doc_count)
        else:
            stop_words = set()

        # list the terms that should be part of the vocabulary
        if max_features is None:
            terms = set(term_counts) - stop_words
        else:
            # extract the most frequent terms for the vocabulary
            terms = set()
            for t, tc in term_counts.most_common():
                if t not in stop_words:
                    terms.add(t)
                if len(terms) >= max_features:
                    break

        return terms, stop_words

    @cached(_transform_cache_key_countvectorizer)
    def _term_counts_per_doc(self, raw_documents):
        analyze = self.build_analyzer()
        i_indices = []
        j_indices = []
        values = []
        vocabulary = self.vocabulary_
        for i, doc in enumerate(raw_documents):
            term_count_current = Counter(analyze(doc))
            for term, count in term_count_current.iteritems():
                j = vocabulary.get(term)
                if j is not None:
                    i_indices.append(i)
                    j_indices.append(j)
                    values.append(count)

        term_counts_per_doc = coo_matrix(
            (values, (i_indices, j_indices)),
            shape=(len(raw_documents), len(self.vocabulary_)),
            dtype=np.float64,
            )

        # print "[LargeCountVectorizer] end of transform."
        return term_counts_per_doc


def _transform_cache_key_tfidftransformer(self, X):
    rows, cols = X.nonzero()
    return ','.join([
        str(rows[:100]),
        str(rows[-100:]),
        str(cols[:100]),
        str(cols[-100:]),
        str(X.shape),
        str(sorted(self.get_params().items())),
        ])


class CachedTfidfTransformer(TfidfTransformer):
    @cached(_transform_cache_key_tfidftransformer)
    def transform(self, X, copy=True):
        return super(CachedTfidfTransformer, self).transform(X, copy)
