import numpy as np


def test_cap_and_update_priors():
    from soggle.competition_utilities import cap_and_update_priors

    old_priors = np.array([0.125, 0.125, 0.125, 0.375, 0.125])
    old_priors = np.array([0.07, 0.07, 0.07, 0.72, 0.07])
    new_priors = np.array([0.01, 0.01, 0.01, 0.96, 0.01])

    old_posteriors = np.array([
        [0.0, 0.0, 0.0, 1.0, 0.0],
        [0.0, 0.0, 0.0, 0.5, 0.5],
        ])

    result = cap_and_update_priors(
        old_priors, old_posteriors, new_priors, 0.001)
    assert result[0, -2] == 0.99601196410767712
    assert result[1, -2] == 0.88206173318081327


def test_multiply_features():
    from soggle.features import multiply_features

    features = multiply_features(np.array([[2, 3], [4, 5]]))
    assert (features == np.array([[2 * 3], [4 * 5]])).all()

    features = multiply_features(np.array([[2, 3, 4, 5],
                                          [6, 7, 8, 9]]))
    expected = np.array([[2 * 3, 2 * 4, 2 * 5, 3 * 4, 3 * 5, 4 * 5],
                         [6 * 7, 6 * 8, 6 * 9, 7 * 8, 7 * 9, 8 * 9]])
    assert (features == expected).all()


def test_multiply_features_with_names():
    from soggle.features import multiply_features

    features, names = multiply_features(
        np.array([[2, 3, 4, 5]]),
        names=('two', 'three', 'four', 'five'),
        )

    expected_features = np.array([[2 * 3, 2 * 4, 2 * 5, 3 * 4, 3 * 5, 4 * 5]])
    expected_names = np.array([
        'two X three', 'two X four', 'two X five',
        'three X four', 'three X five',
        'four X five',
        ])
    assert (features == expected_features).all()
    assert (names == expected_names).all()
