======
soggle
======

Code for the `Predict Closed Questions on Stack Overflow
<https://www.kaggle.com/c/predict-closed-questions-on-stack-overflow>`_
challenge on kaggle.


Installation
============

Installation requires ``virtualenv``.

To install ``soggle`` in development mode, change into the directory
containing this ``README.txt`` file and do::

  $ virtualenv . --system-site-packages
  $ bin/python setup.py develop

You may have additional dependencies such as ``numpy`` and
``matplotlib`` already installed through your system's package
manager.  If you want to install these extra dependencies as part of
the installation, do::

  $ SOGGLE_INSTALL_ALL_DEPS=1 bin/python setup.py develop


Data
====

``soggle`` uses the same CSV format as the original training data
provided in the competition.

More datasets to work with are are available online.  To download and
unzip the data, do::

  $ wget http://danielnouri.org/files/soggle-data.tar.bz2
  $ tar xjfv soggle-data.tar.bz2

This will create a ``data`` folder with all the necessary datasets.
The individual datasets are explained in the ``data/README.txt`` file.

The md5sum of soggle-data.tar.bz2 is 9d2a7867d1fea4c4741da65d05b714d5.

For the final submission, you will also need to put the original
``train.csv`` into the ``data/`` folder.


Submit
======

To produce a submission file, use the ``submit-final.sh`` script.
This will create a file called ``submission.csv``, which you can
upload.  See the ``submit-final.sh`` file for a list of parameters.


Console commands
================

soggle-introspect
-----------------

Use the ``soggle-introspect`` command to find out what's in a given
dataset file.  This will print the amount of samples for each class,
and a total, along with date ranges for each class.  An example::

  $ soggle-introspect data/train-sample-reverse.csv 


soggle-learn
============

The ``soggle-learn eval_mode`` command allows you to run a number of
models against a given dataset, and evaluate them using
cross-validation.

Here's an example of how to invoke ``eval_mode``::

  $ soggle-learn eval_model data/train-sample-reverse-small.csv 

A number of ``soggle-learn grid_search`` commands are available.
These run the various grid search functions that you can find in
``soggle/learn.py``, to help you find the optimal parameters for the
different models.  Try this::

  $ soggle-learn grid_search_base data/train-sample-reverse-small.csv

You can also display a learning curve for a given model::

  $ soggle-learn learning_curve --model=0 data/train-sample-reverse-small.csv

By setting ``--model=0``, we ask for a learning curve for the 0 model,
i.e. the first base model.  (This is the default.)

Once you're finished with tuning the parameters, you can use
``soggle-learn train`` to train your classifier with the given
dataset, and store the classifier to disk, in pickle format.  An
example::

  $ soggle-learn train data/train-sample-reverse-small.csv model-sample-reverse-small.pkl --truncate=0.175,1.0

Another example::

  $ soggle-learn train data/train-sample-reverse.csv model-sample-reverse-2.pkl --truncate=0.147,1.0

Note that in the above examples, we truncate the training set so that
it has no overlap with the ``train-30k.csv`` file that we use in the
next step to evaluate our model.


soggle-eval
===========

With ``soggle-eval``, it is possible to evaluate your classifiers
trained with ``soggle-learn train``.

We can calculate the f1-score and the multiclass log loss using
``soggle-eval logloss``::

  $ soggle-eval logloss data/train-30k.csv model-sample-reverse-small.pkl

If you want ``soggle-eval logloss`` to update the priors, as is done
in the original ``basic_benchmark.py`` of the competition, you must
pass both the ``--full-train-file`` and the ``--train-file`` as
arguments.  An example::

  $ soggle-eval logloss data/train-30k.csv model-sample-reverse-small.pkl \
        --full-train-file=data/train.csv \
        --train-file=data/train-sample-reverse-small.csv 

Finally, to write the submission file, use the command as above,
change the ``logloss`` to ``write_submission`` and add a third file,
the file to write out to::

  $ soggle-eval submit \
        data/public_leaderboard.csv \
        model-sample-reverse-small.pkl \
        submission.csv \
        --full-train-file=data/train-300k.csv \
        --train-file=data/train-sample-reverse-small.csv 
